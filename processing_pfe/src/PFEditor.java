import processing.core.PApplet;

public class PFEditor extends PApplet {

	PFModel model;// PFモデル
	int selectedId;// フォーカス中のオブジェクトID
	int mouseXbuffer;// 移動時バッファ
	int mouseYbuffer;// 移動時バッファ
	boolean shiftkey;// shiftキー押下判定

	public void setup() {
		size(800, 600, "processing.core.PGraphicsRetina2D");
		noLoop();

		// データのロード
		selectedId = -1;
		model = new PFModel();
	}

	public void draw() {
		background(252);
		// draw Interface
		for (Interface i : model.interfaces) {
			stroke(150, 150, 150);
			if (selectedId == i.id)
				stroke(255, 0, 0);
			int rootX = model.getNodeXById(i.rootObjectId);
			int rootY = model.getNodeYById(i.rootObjectId);
			int distX = model.getNodeXById(i.distObjectId);
			int distY = model.getNodeYById(i.distObjectId);
			line(rootX, rootY, distX, distY);
			noStroke();
			fill(100, 100, 100);
			text(i.name, rootX / 2 + distX / 2, rootY / 2 + distY / 2);
		}
		// draw reqs
		for (Requirement r : model.requirements) {
			noStroke();
			fill(200, 255, 128);
			if (r.id == selectedId)
				stroke(255, 0, 0);
			ellipse(r.x, r.y, 50, 50);
			fill(100, 100, 100);
			text(r.name, r.x, r.y);
		}
		// draw doms
		for (Domain d : model.domains) {
			noStroke();
			fill(128, 200, 255);
			if (d.id == selectedId)
				stroke(255, 0, 0);
			rect(d.x - 20, d.y - 20, 40, 40);
			noStroke();
			fill(100, 100, 100);
			text(d.name, d.x, d.y);
		}
	}

	public void keyPressed() {
		if (keyCode == SHIFT) {
			shiftkey = true;
		} else if (selectedId == -1) {// コマンドモード
			if (key == 'r') {
				Requirement r = new Requirement();
				r.x = mouseX;
				r.y = mouseY;
				r.name = "requirement";
				model.add(r);
			} else if (key == 'd') {
				Domain d = new Domain();
				d.x = mouseX;
				d.y = mouseY;
				d.name = "domain";
				model.add(d);
			} else if (key == 'q') {
				exit();
			} else if (key == 's') {
				XML.save(model);
			} else if (key == 'l') {
				model = XML.load();
				selectedId = -1;
			}
		} else if (key == DELETE) {// 選択したモデルの削除
			model.remove(selectedId);
			selectedId = -1;
		} else {// キー入力
			model.rename(selectedId, key);
		}
		redraw();
	}

	public void keyReleased() {
		if (keyCode == SHIFT) {
			shiftkey = false;
		}
	}

	public void mousePressed() {
		if (mouseButton == LEFT) {
			int nowId = model.hasNode(mouseX, mouseY);
			if (nowId == -1) {
				selectedId = -1;
			} else if (shiftkey == true && selectedId != -1
					&& selectedId != nowId) {
				int edgeId = model.hasEdge(selectedId, nowId);
				int rEdgeId = model.hasEdge(nowId, selectedId);
				if (edgeId != -1) {// 貼ろうとしてるEdgeが既にある場合
					selectedId = edgeId;
				} else if (rEdgeId != -1) {
					selectedId = rEdgeId;
				} else if (model.getNodeXById(selectedId) != -1) {
					Interface i = new Interface();
					i.name = "interface";
					i.rootObjectId = selectedId;
					i.distObjectId = nowId;
					model.add(i);
					selectedId = i.id;
				}
			} else {
				selectedId = nowId;
				mouseXbuffer = mouseX;
				mouseYbuffer = mouseY;
			}
		}
		redraw();
	}

	public void mouseDragged() {
		if (mouseButton == LEFT) {
			if (selectedId != -1) {
				model.moveNodeById(selectedId, mouseX - mouseXbuffer, mouseY
						- mouseYbuffer);
				mouseXbuffer = mouseX;
				mouseYbuffer = mouseY;
			}
		}
		redraw();
	}

	static public void main(String[] passedArgs) {
		String[] appletArgs = new String[] { "PFEditor" };
		if (passedArgs != null) {
			PApplet.main(concat(appletArgs, passedArgs));
		} else {
			PApplet.main(appletArgs);
		}
	}
}

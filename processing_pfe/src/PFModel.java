import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import processing.core.PApplet;

class PFModel {
	public List<Requirement> requirements;
	public List<Domain> domains;
	public List<Interface> interfaces;
	public int maxId;

	public PFModel() {
		requirements = Collections.synchronizedList(new ArrayList<Requirement>());
		domains = Collections.synchronizedList(new ArrayList<Domain>());
		interfaces = Collections.synchronizedList(new ArrayList<Interface>());
		maxId = 0;
	}

	/** オブジェクトの追加 **/
	public void add(Object obj) {
		if (obj instanceof Requirement) {
			((Requirement) obj).id = ++maxId;
			requirements.add((Requirement) obj);
		} else if (obj instanceof Domain) {
			((Domain) obj).id = ++maxId;
			domains.add((Domain) obj);
		} else if (obj instanceof Interface) {
			((Interface) obj).id = ++maxId;
			interfaces.add((Interface) obj);
		}
	}

	/** オブジェクトの削除 **/
	public void remove(int id) {
		// for eachでのリムーブはおこだった
		for (int i = 0; i < requirements.size(); i++) {
			if (requirements.get(i).id == id)
				requirements.remove(i);
		}
		for (int i = 0; i < domains.size(); i++) {
			if (domains.get(i).id == id)
				domains.remove(i);
		}
		for (int i = 0; i < interfaces.size();) {
			if (interfaces.get(i).id == id || interfaces.get(i).distObjectId == id
					|| interfaces.get(i).rootObjectId == id) {
				interfaces.remove(i);
			} else {
				i++;
			}
		}
	}

	/** 指定場所になんのNodeが存在しているか **/
	public int hasNode(int x, int y) {
		/** オブジェクト選択範囲のマージン **/
		final int mergin = 30;

		for (Requirement r : requirements) {
			if (r.x - mergin < x && x < r.x + mergin && r.y - mergin < y
					&& y < r.y + mergin)
				return r.id;
		}
		for (Domain r : domains) {
			if (r.x - mergin < x && x < r.x + mergin && r.y - mergin < y
					&& y < r.y + mergin)
				return r.id;
		}
		return -1;
	}

	/** 指定したrootID,distIDに存在するEdgeを返す **/
	public int hasEdge(int rootId, int distId) {
		for (Interface i : interfaces) {
			if (i.rootObjectId == rootId && i.distObjectId == distId) {
				return i.id;
			}
		}
		return -1;
	}

	/** 指定IDのNodeの座標値（X）を取得 **/
	public int getNodeXById(int id) {
		for (Requirement r : requirements) {
			if (r.id == id)
				return r.x;
		}
		for (Domain d : domains) {
			if (d.id == id)
				return d.x;
		}
		return -1;
	}

	/** 指定IDのNodeの座標値（Y）を取得 **/
	public int getNodeYById(int id) {
		for (Requirement r : requirements) {
			if (r.id == id)
				return r.y;
		}
		for (Domain d : domains) {
			if (d.id == id)
				return d.y;
		}
		return -1;
	}

	/** 指定IDのNodeを移動 **/
	public void moveNodeById(int id, int x, int y) {
		for (Requirement r : requirements) {
			if (r.id == id) {
				r.x += x;
				r.y += y;
				return;
			}
		}
		for (Domain d : domains) {
			if (d.id == id) {
				d.x += x;
				d.y += y;
				return;
			}
		}
	}

	/** 指定IDを持つモデルの名前を変更 **/
	public void rename(int id, char c) {
		for (Requirement r : requirements) {
			if (r.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (r.name.length() > 0)
						r.name = r.name.substring(0, r.name.length() - 1);
					return;
				} else {
					r.name += c;
				}
			}
		}
		for (Domain d : domains) {
			if (d.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (d.name.length() > 0)
						d.name = d.name.substring(0, d.name.length() - 1);
				} else {
					d.name += c;
				}
			}
		}
		for (Interface i : interfaces) {
			if (i.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (i.name.length() > 0)
						i.name = i.name.substring(0, i.name.length() - 1);
				} else {
					i.name += c;
				}
			}
		}
	}
}

class Domain {
	public int id;
	public String name;
	public String domainType;
	public int x, y;
}

class Requirement {
	public int id;
	public String name;
	public int x, y;
}

class Interface {
	public int id;
	public String name;
	public int rootObjectId, distObjectId;
	public boolean hasDirected;
}

import java.io.File;

import javax.xml.bind.JAXB;

public class XML {

	static File file = new File("test.xml");

	public static PFModel load() {
		return JAXB.unmarshal(file, PFModel.class);
	}

	public static void save(PFModel model) {
		JAXB.marshal(model, file);
	}
}

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import processing.core.PApplet;

class Model {
	public List<Requirement> req;
	public List<Domain> dom;
	public List<Interface> ifs;

	public Model() {
		req = Collections.synchronizedList(new ArrayList<Requirement>());
		dom = Collections.synchronizedList(new ArrayList<Domain>());
		ifs = Collections.synchronizedList(new ArrayList<Interface>());
	}

	/** オブジェクトの追加 **/
	public void add(Object obj) {
		if (obj instanceof Requirement) {
			req.add((Requirement) obj);
		} else if (obj instanceof Domain) {
			dom.add((Domain) obj);
		} else if (obj instanceof Interface) {
			ifs.add((Interface) obj);
		}
	}

	/** オブジェクトの削除 **/
	public void remove(int id) {
		// for eachでのリムーブはおこだった
		for (int i = 0; i < req.size(); i++) {
			if (req.get(i).id == id)
				req.remove(i);
		}
		for (int i = 0; i < dom.size(); i++) {
			if (dom.get(i).id == id)
				dom.remove(i);
		}
		for (int i = 0; i < ifs.size(); i++) {
			if (ifs.get(i).id == id || ifs.get(i).distObjectId == id
					|| ifs.get(i).rootObjectId == id) {
				ifs.remove(i);
				System.out.println("rm:" + i);
			} else {
				System.out.println("nr:");
			}
		}
	}

	/** 指定場所になんのNodeが存在しているか **/
	public int hasNode(int x, int y) {
		/** オブジェクト選択範囲のマージン **/
		final int mergin = 30;

		for (Requirement r : req) {
			if (r.x - mergin < x && x < r.x + mergin && r.y - mergin < y
					&& y < r.y + mergin)
				return r.id;
		}
		for (Domain r : dom) {
			if (r.x - mergin < x && x < r.x + mergin && r.y - mergin < y
					&& y < r.y + mergin)
				return r.id;
		}
		return -1;
	}

	/** 指定したrootID,distIDに存在するEdgeを返す **/
	public int hasEdge(int rootId, int distId) {
		for (Interface i : ifs) {
			if (i.rootObjectId == rootId && i.distObjectId == distId) {
				return i.id;
			}
		}
		return -1;
	}

	/** 指定IDのNodeの座標値（X）を取得 **/
	public int getNodeXById(int id) {
		for (Requirement r : req) {
			if (r.id == id)
				return r.x;
		}
		for (Domain d : dom) {
			if (d.id == id)
				return d.x;
		}
		return -1;
	}

	/** 指定IDのNodeの座標値（Y）を取得 **/
	public int getNodeYById(int id) {
		for (Requirement r : req) {
			if (r.id == id)
				return r.y;
		}
		for (Domain d : dom) {
			if (d.id == id)
				return d.y;
		}
		return -1;
	}

	/** 指定IDのNodeを移動 **/
	public void moveNodeById(int id, int x, int y) {
		for (Requirement r : req) {
			if (r.id == id) {
				r.x += x;
				r.y += y;
				return;
			}
		}
		for (Domain d : dom) {
			if (d.id == id) {
				d.x += x;
				d.y += y;
				return;
			}
		}
	}

	/** 指定IDを持つモデルの名前を変更 **/
	public void rename(int id, char c) {
		for (Requirement r : req) {
			if (r.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (r.name.length() > 0)
						r.name = r.name.substring(0, r.name.length() - 1);
					return;
				} else {
					r.name += c;
				}
			}
		}
		for (Domain d : dom) {
			if (d.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (d.name.length() > 0)
						d.name = d.name.substring(0, d.name.length() - 1);
				} else {
					d.name += c;
				}
			}
		}
		for (Interface i : ifs) {
			if (i.id == id) {
				if (c == PApplet.BACKSPACE) {
					if (i.name.length() > 0)
						i.name = i.name.substring(0, i.name.length() - 1);
				} else {
					i.name += c;
				}
			}
		}
	}
}

class Domain {
	public int id;
	public String name;
	public String domainType;
	public int x, y;
}

class Requirement {
	public int id;
	public String name;
	public int x, y;
}

class Interface {
	public int id;
	public String name;
	public int rootObjectId, distObjectId;
	public boolean hasDirected;
}
